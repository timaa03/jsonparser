<?php

class JsonParser
{
    private $currentIndex = 0;

    private $char = ' ';

    private $text;

    /**
     * @param string $message
     * @throws ParseError
     */
    private function error(string $message)
    {
        throw new ParseError($message);

    }

    /**
     * @param null | string $ch
     * @return string
     */
    private function next($ch = null)
    {
        if ($ch && $ch !== $this->char) {
            $this->error(sprintf("expected %s instead of %s", $ch, $this->char));
        }

        $this->char = mb_substr($this->text, $this->currentIndex, 1);
        $this->currentIndex++;

        return $this->char;
    }

    /**
     * @return int|float
     */
    private function getNumber()
    {
        $string = "";

        if ($this->char === "-") {
            $string = "-";
            $this->next("-");
        }
        while ($this->char >= "0" && $this->char <= "9") {
            $string .= $this->char;
            $this->next();
        }
        if ($this->char == ".") {
            $string .= '.';
            while ($this->next() && $this->char >= '0' && $this->char <= '9') {
                $string .= $this->char;
            }
        }

        if (!is_numeric($string)) {
            $this->error("Bad number");
        }
        $number = $string;

        return $number;
    }

    /**
     * @return string
     */
    private function getString()
    {
        $string = "";

        if ($this->char === '"') {
            while ($this->next()) {
                if ($this->char === '"') {
                    $this->next();
                    return $string;
                } else {
                    $string .= $this->char;
                }
            }
        }
        return $string;
    }

    /**
     * @return array
     */
    private function getObject()
    {
        $object = [];

        if ($this->char === '{') {
            $this->next('{');
            $this->white();
            if ($this->char === '}') {
                $this->next('}');
                return $object;
            }
            while ($this->char) {
                $key = $this->getString();
                $this->white();
                $this->next(':');
                $object[$key] = $this->value();
                $this->white();
                if ($this->char === '}') {
                    $this->next('}');
                    return $object;
                }
                $this->next(',');
                $this->white();
            }
        }
        $this->error("Bad object");

        return $object;
    }

    /**
     * @return bool|null
     */
    private function  getReservedWord()
    {
        switch ($this->char) {
            case 't':
                $this->next('t');
                $this->next('r');
                $this->next('u');
                $this->next('e');
                return true;
            case 'f':
                $this->next('f');
                $this->next('a');
                $this->next('l');
                $this->next('s');
                $this->next('e');
                return false;
            case 'n':
                $this->next('n');
                $this->next('u');
                $this->next('l');
                $this->next('l');
                return null;
        }
        $this->error(sprintf("Unexpected  %s ", $this->char));
    }

    /**
     * @return array
     */
    private function getArray()
    {
        $res = [];

        if ($this->char === '[') {
            $this->next('[');
            $this->white();
            if ($this->char === ']') {
                $this->next(']');
                return $res;   // empty array
            }
            while ($this->char) {
                array_push($res, $this->value());
                $this->white();
                if ($this->char === ']') {
                    $this->next(']');
                    return $res;
                }
                $this->next(',');
                $this->white();
            }
        }
        $this->error("Bad array");

        return $res;
    }

    private function white()
    {
        while ($this->char && $this->char <= ' ' && $this->char = "'") {
            $this->next();
        }
    }

    /**
     * @return array|float|int|string
     */
    private function value()
    {
        $this->white();
        switch ($this->char) {
            case '{':
                return $this->getObject();
            case '[':
                return $this->getArray();
            case '"':
                return $this->getString();
            case '-':
                return $this->getNumber();
            default:
                return $this->char >= '0' && $this->char <= '9' ? $this->getNumber() : $this->getReservedWord();
        }
    }

    /**
     * @param $text
     * @return array|float|int|string
     */
    public function parse($text)
    {
        $this->text = $text;
        return $this->value();
    }
}

$parser = new JsonParser();
var_dump($parser->parse('{"name":"John", "innerObj":{"oolo":"val1", "row2": true, "row3": null}, "age":30, "array":[1,2,3]}'));